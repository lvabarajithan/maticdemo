package com.matic.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.matic.demo.util.CryptUtil;
import com.matic.demo.util.HashGen;
import com.matic.demo.util.PrefsUtil;

/**
 * Created by Abarajithan
 */
public class NewAccountActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatEditText usernameView;
    private AppCompatEditText passwordView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);

        usernameView = findViewById(R.id.new_account_username_view);
        passwordView = findViewById(R.id.new_account_password_view);
        findViewById(R.id.new_account_create_btn).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        String username = usernameView.getText().toString();
        String password = passwordView.getText().toString();

        if (username.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "All detail(s) required", Toast.LENGTH_SHORT).show();
        } else {
            try {
                createAccount(username, password);
            } catch (Exception e) {
                Toast.makeText(this, "Cannot create account", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

    }

    private void createAccount(String username, String password) throws Exception {
        String eh = CryptUtil.encrypt(HashGen.nextRandom(), username, password);
        if (eh == null) {
            Toast.makeText(this, "Account already exists", Toast.LENGTH_SHORT).show();
            return;
        }
        PrefsUtil.saveHash(eh);
        PrefsUtil.saveAccount(username, password);
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
