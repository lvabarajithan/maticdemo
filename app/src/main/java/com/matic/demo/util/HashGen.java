package com.matic.demo.util;

import java.security.SecureRandom;

/**
 * Created by Abarajithan
 */
public class HashGen {

    private static final int DEFAULT_LENGTH = 20;

    private static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LOWER = UPPER.toLowerCase();
    private static final String NUMERIC = "0123456789";
    private static final String ALPHA_NUMERIC = UPPER + LOWER + NUMERIC;

    public static String nextRandom() {
        SecureRandom random = new SecureRandom();
        char[] buf = new char[DEFAULT_LENGTH];
        char[] symbols = ALPHA_NUMERIC.toCharArray();
        for (int i = 0; i < DEFAULT_LENGTH; i++) {
            buf[i] = symbols[random.nextInt(symbols.length)];
        }
        return new String(buf);
    }

}
