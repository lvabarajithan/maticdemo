package com.matic.demo.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Map;

/**
 * Created by Abarajithan
 */
public class PrefsUtil {

    private static SharedPreferences prefs;

    public static void init(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().remove("session").apply();
        for (Map.Entry entry : prefs.getAll().entrySet()) {
            System.out.printf("%s : %s\n", entry.getKey(), entry.getValue());
        }
    }

    public static void saveHash(String hash) {
        prefs.edit().putString("hash", hash).apply();
    }

    public static String getHash() {
        return prefs.getString("hash", null);
    }

    public static void saveAccount(String username, String password) {
        prefs.edit().putString(username, password).apply();
    }

    public static String getAccount(String username) {
        return prefs.getString(username, null);
    }

    public static void setSession(String username) {
        prefs.edit().putString("session", username).apply();
    }

    public static String getSession() {
        return prefs.getString("session", null);
    }
}
