package com.matic.demo;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.matic.demo.adapter.OnTouchListener;
import com.matic.demo.adapter.RecyclerTouchListener;
import com.matic.demo.adapter.TokensAdapter;
import com.matic.demo.model.Token;
import com.matic.demo.util.CryptUtil;
import com.matic.demo.util.PrefsUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abarajithan
 */
public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private static final List<Token> SAMPLE_DATA = new ArrayList<Token>() {{
        add(new Token("BCC", "bcc", 100.5, "bcc.png"));
        add(new Token("BitCoin Diamond", "bcd", 10.5, "bcd.png"));
        add(new Token("BitCoin Cash", "bch", 1135.5, "bch.png"));
        add(new Token("Blockchain.io", "bcio", 81.5, "bcio.png"));
        add(new Token("Bytecoin", "bcn", 134.5, "bcn.png"));
    }};

    private AlertDialog dialog = null;
    private AppCompatImageView imageView;
    private AppCompatTextView nameView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setupDialog();

        findViewById(R.id.home_show_hash_btn).setOnClickListener(this);

        RecyclerView tokensView = findViewById(R.id.home_tokens_list);
        tokensView.setHasFixedSize(true);
        tokensView.setItemAnimator(new DefaultItemAnimator());
        tokensView.setLayoutManager(new LinearLayoutManager(this));
        tokensView.setAdapter(new TokensAdapter(SAMPLE_DATA));
        tokensView.addOnItemTouchListener(new RecyclerTouchListener(this, tokensView, new OnTouchListener() {
            @Override
            public void onTouchAndHold(int position) {
                Token token = SAMPLE_DATA.get(position % SAMPLE_DATA.size());
                nameView.setText(token.getName());
                Glide.with(HomeActivity.this).load(Uri.parse("file:///android_asset/" + token.getImageName())).into(imageView);
                dialog.show();
            }

            @Override
            public void onRelease() {
                dialog.dismiss();
            }
        }));
    }

    private void setupDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View dialogView = getLayoutInflater().inflate(R.layout.layout_dialog_token_preview, null);
        nameView = dialogView.findViewById(R.id.dialog_token_preview_name);
        imageView = dialogView.findViewById(R.id.dialog_token_preview_image);

        builder.setView(dialogView);
        dialog = builder.create();
    }

    @Override
    public void onClick(View v) {
        try {
            String eh = PrefsUtil.getHash();
            String uname = PrefsUtil.getSession();
            String pass = PrefsUtil.getAccount(uname);
            String hash = CryptUtil.decrypt(eh, uname, pass);
            new AlertDialog.Builder(this)
                    .setTitle("Hash")
                    .setMessage(hash)
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
        } catch (Exception e) {
            Toast.makeText(this, "Cannot retrieve hash", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

}
