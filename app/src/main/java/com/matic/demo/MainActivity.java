package com.matic.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Abarajithan
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.main_new_accnt_btn).setOnClickListener(this);
        findViewById(R.id.main_sign_in_btn).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_new_accnt_btn:
                startActivity(new Intent(this, NewAccountActivity.class));
                break;
            case R.id.main_sign_in_btn:
                startActivity(new Intent(this, SignInActivity.class));
                break;
        }
    }
}
