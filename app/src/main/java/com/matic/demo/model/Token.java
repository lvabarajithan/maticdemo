package com.matic.demo.model;

/**
 * Created by Abarajithan
 */
public class Token {

    private String name;

    private String token;

    private double value;

    private String imageName;

    public Token(String name, String token, double value, String imageName) {
        this.name = name;
        this.token = token;
        this.value = value;
        this.imageName = imageName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

}
