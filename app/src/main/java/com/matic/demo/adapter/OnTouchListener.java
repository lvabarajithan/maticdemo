package com.matic.demo.adapter;

/**
 * Created by Abarajithan
 */
public interface OnTouchListener {
    public void onTouchAndHold(int position);

    public void onRelease();
}
