package com.matic.demo.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.matic.demo.R;
import com.matic.demo.model.Token;

import java.util.List;

/**
 * Created by Abarajithan
 */
public class TokensAdapter extends RecyclerView.Adapter<TokensAdapter.TokenHolder> {

    private static final int SAMPLE_DATA_LENGTH = 20;

    private Context context;
    private List<Token> tokenList;
    private int tokenCount;

    public TokensAdapter(List<Token> tokenList) {
        this.tokenList = tokenList;
        this.tokenCount = tokenList.size();
    }

    @NonNull
    @Override
    public TokenHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_token_list, parent, false);
        return new TokenHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TokenHolder holder, int position) {
        final Token token = tokenList.get(position % tokenCount);
        holder.nameView.setText(token.getName());
        holder.tokenView.setText(token.getToken());
        holder.valueView.setText(String.valueOf(token.getValue()));
        Glide.with(context).load(Uri.parse("file:///android_asset/" + token.getImageName())).into(holder.imgView);
    }

    @Override
    public int getItemCount() {
        return SAMPLE_DATA_LENGTH;
    }

    class TokenHolder extends RecyclerView.ViewHolder {
        AppCompatTextView nameView;
        AppCompatTextView tokenView;
        AppCompatTextView valueView;
        AppCompatImageView imgView;

        TokenHolder(@NonNull View itemView) {
            super(itemView);
            this.nameView = itemView.findViewById(R.id.item_token_list_name);
            this.tokenView = itemView.findViewById(R.id.item_token_list_token);
            this.valueView = itemView.findViewById(R.id.item_token_list_value);
            this.imgView = itemView.findViewById(R.id.item_token_list_image);
        }
    }

}
