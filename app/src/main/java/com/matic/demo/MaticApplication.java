package com.matic.demo;

import android.app.Application;

import com.matic.demo.util.PrefsUtil;

/**
 * Created by Abarajithan
 */
public class MaticApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        PrefsUtil.init(this);
    }

}
