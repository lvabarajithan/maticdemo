package com.matic.demo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.matic.demo.util.PrefsUtil;

/**
 * Created by Abarajithan
 */
public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatEditText usernameView;
    private AppCompatEditText passwordView;

    private SharedPreferences prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        this.prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        usernameView = findViewById(R.id.sign_in_username_view);
        passwordView = findViewById(R.id.sign_in_password_view);
        findViewById(R.id.sign_in_create_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String username = usernameView.getText().toString();
        String password = passwordView.getText().toString();

        if (username.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "All detail(s) required", Toast.LENGTH_SHORT).show();
        } else {
            try {
                signIn(username, password);
            } catch (Exception e) {
                Toast.makeText(this, "Cannot sign in", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    private void signIn(String username, String password) throws Exception {
        String pass = PrefsUtil.getAccount(username);
        if (pass == null || !pass.contentEquals(password)) {
            Toast.makeText(this, "Cannot sign you in", Toast.LENGTH_SHORT).show();
            return;
        }
        PrefsUtil.setSession(username);
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
